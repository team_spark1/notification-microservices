package com.spark.notificationmicroservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationMicroservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotificationMicroservicesApplication.class, args);
	}

}
